package com.hugeinc.lambda

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsyncClient
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper
import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.events.SNSEvent
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.ObjectMetadata
import com.hugeinc.model.Viewport
import com.hugeinc.model.dynamodb.CaptureItem
import com.hugeinc.model.dynamodb.Screenshot
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.URIBuilder

import java.text.SimpleDateFormat

/**
 * User: HUGE-gilbert
 * Date: 1/21/16
 * Time: 3:19 PM
 */
class ScreenCapture {
    private static final String PHANTOM_JS_CLOUD_HOST = 'http://api.phantomjscloud.com'
    private static final String PHANTOM_JS_CLOUD_PATH = '/single/browser/v1/54b460f608e147b4c5321b6046ab29ad610eec99'
    private static final String IMAGE_BUCKET = 'capture-lambda'
    private static final String S3_PUBLIC_DOMAIN = 'https://s3.amazonaws.com'

    private HTTPBuilder http = new HTTPBuilder(PHANTOM_JS_CLOUD_HOST)
    private AmazonS3 s3Client = new AmazonS3Client()
    private AmazonDynamoDB dynamoDB = new AmazonDynamoDBAsyncClient()
    private DynamoDBMapper dynamoDBMapper = new DynamoDBMapper(dynamoDB)

    /**
     * Lambda function handler, receive and event with the url and generates three screenshots, one for every viewport:
     * small, medium, large. Stores the result on dynamo db CaptureHistory table
     */
    void handleRequest(SNSEvent event, Context context) {
        def requestBody = ['targetUrl': event.records[0].sns.message, 'requestType': 'jpeg', 'loadImages': true]

        URI uri = requestBody.targetUrl.toURI()
        URIBuilder uriBuilder = new URIBuilder(uri)
        uriBuilder.userInfo = null
        String url = uriBuilder.toString()
        context.logger.log('Url: ' + url)

        // Current date
        Date now = new Date()
        Screenshot screenshot = new Screenshot(date: now)

        Viewport.values().each { viewport ->
            requestBody.viewportSize = ['width': viewport.width]
            http.post(path: PHANTOM_JS_CLOUD_PATH, body: requestBody, contentType: ContentType.BINARY,
                    requestContentType: ContentType.JSON) { resp, inputStream ->
                println "POST Success: ${resp.statusLine}"
                context.logger.log(String.valueOf(resp.statusLine.statusCode))

                String nowString = new SimpleDateFormat('yyyy_MM_dd_hh:mm').format(now)
                def fileName = url.toString().replace('/', '_') + '/' + viewport.field + '_' + nowString + '.jpg'

                // Upload the generated screen capture to S3
                s3Client.putObject(IMAGE_BUCKET, fileName, inputStream, new ObjectMetadata())
                // Add the viewport to the screenshot
                screenshot.metaClass.setProperty(screenshot, viewport.field, "$S3_PUBLIC_DOMAIN/$IMAGE_BUCKET/$fileName")
            }
        }

        saveCapture(url, screenshot)
    }

    /**
     * Stores the new screenshot into dynamodb table
     * @param url
     * @param screenshot
     * @return
     */
    private void saveCapture(String url, Screenshot screenshot) {
        // Verify if the item exists
        CaptureItem item = dynamoDBMapper.load(CaptureItem.class, url)
        // Add screenshot to the item
        if (item) {
            item.screenshots.add(screenshot)
        } else {
            item = new CaptureItem(url: url, screenshots: [screenshot])
        }

        dynamoDBMapper.save(item)
    }

}
