package com.hugeinc.model

/**
 * User: HUGE-gilbert
 * Date: 1/21/16
 * Time: 3:17 PM
 */
enum Viewport {
    SMALL(320), MEDIUM(640), LARGE(960)

    int width

    Viewport(int width) {
        this.width = width
    }

    String getField() {
        this.name().toLowerCase()
    }
}