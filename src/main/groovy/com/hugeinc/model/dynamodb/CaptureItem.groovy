package com.hugeinc.model.dynamodb

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMarshalling
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable

/**
 * User: HUGE-gilbert
 * Date: 1/26/16
 * Time: 11:42 AM
 */
@DynamoDBTable(tableName = 'CaptureHistory')
class CaptureItem implements Serializable {
    @DynamoDBHashKey(attributeName = 'URL')
    String url

    @DynamoDBAttribute(attributeName = "Screenshots")
    @DynamoDBMarshalling(marshallerClass = ScreenshotMarshaller.class)
    List<Screenshot> screenshots
}
