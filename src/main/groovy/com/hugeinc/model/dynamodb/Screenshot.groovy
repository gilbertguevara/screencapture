package com.hugeinc.model.dynamodb

/**
 * User: HUGE-gilbert
 * Date: 1/26/16
 * Time: 11:45 AM
 */
class Screenshot implements Serializable {
    Date date
    String small
    String medium
    String large
}
