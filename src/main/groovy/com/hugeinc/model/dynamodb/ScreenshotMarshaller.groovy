package com.hugeinc.model.dynamodb

import com.amazonaws.services.dynamodbv2.datamodeling.JsonMarshaller

/**
 * User: HUGE-gilbert
 * Date: 1/26/16
 * Time: 11:47 AM
 */
class ScreenshotMarshaller extends JsonMarshaller<Screenshot> {
}
