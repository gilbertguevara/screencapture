package com.hugeinc

import com.amazonaws.services.lambda.runtime.ClientContext
import com.amazonaws.services.lambda.runtime.CognitoIdentity
import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.LambdaLogger
import com.amazonaws.services.lambda.runtime.events.SNSEvent
import com.hugeinc.lambda.ScreenCapture

/**
 * User: HUGE-gilbert
 * Date: 1/26/16
 * Time: 10:58 AM
 */
class ScreenCaptureMain {
    public static void main(String[] args) {
        SNSEvent event = new SNSEvent(records: [new SNSEvent.SNSRecord(sns: new SNSEvent.SNS(message: "https://www.scion.com")) ]);
        Context context = new Context() {
            @Override
            String getAwsRequestId() {
                return null
            }

            @Override
            String getLogGroupName() {
                return null
            }

            @Override
            String getLogStreamName() {
                return null
            }

            @Override
            String getFunctionName() {
                return null
            }

            @Override
            CognitoIdentity getIdentity() {
                return null
            }

            @Override
            ClientContext getClientContext() {
                return null
            }

            @Override
            int getRemainingTimeInMillis() {
                return 0
            }

            @Override
            int getMemoryLimitInMB() {
                return 0
            }

            @Override
            LambdaLogger getLogger() {
                return new LambdaLogger() {
                    void log(String message) {
                        System.out.println(message)
                    }
                }
            }
        }
        new ScreenCapture().handleRequest(event, context)
    }
}
